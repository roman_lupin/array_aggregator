// Date: 12 December 2019
// Description: this program was compiled and tested in CLion 2019.1 by g++ 5.4.0 with enabled option 'C++17'
//              under Ubuntu 16.04.6 LTS.

#include <iostream>
#include <limits>
#include <memory>

enum class ArrayToChoose { Small = 0, Big };

template< class T, std::size_t BaseSize >
class ArrayAggregator {
public:
    ArrayAggregator() {
        static_assert( std::is_copy_assignable< T >::value, "array element type must be a copy assignable type" );
        static_assert( ( BaseSize > 0 ), "base size must be greater than zero" );

        constexpr const auto max = std::numeric_limits< std::size_t >::max();
        constexpr const bool condition = ( ( max / BaseSize ) >= BaseSize );
        static_assert( condition, "base size must be less than upper threshold" );

        m_smallArray.size = BaseSize;
        m_smallArray.data = std::make_unique< T[] >( m_smallArray.size );
        m_smallArray.start_index = 0;

        m_bigArray.size = BaseSize * BaseSize;
        m_bigArray.data = std::make_unique< T[] >( m_bigArray.size );
        m_bigArray.start_index = 0;
    }

    ArrayAggregator( ArrayAggregator&& other ) :
        m_smallArray( std::move( other.m_smallArray ) ),
        m_bigArray( std::move( other.m_bigArray ) )
    {
        other.m_smallArray = ArrayParams();
        other.m_bigArray = ArrayParams();
    }

    ArrayAggregator& operator=( ArrayAggregator&& other ) {
        m_smallArray = std::move( other.m_smallArray );
        m_bigArray = std::move( other.m_bigArray );
        other.m_smallArray = ArrayParams();
        other.m_bigArray = ArrayParams();
        return *this;
    }

    ArrayAggregator( const ArrayAggregator& other ) = delete;
    ArrayAggregator& operator=( const ArrayAggregator& other ) = delete;
    ~ArrayAggregator() = default;

    bool addElement( ArrayToChoose arrayToChoose, const T& element ) {
        if ( arrayToChoose == ArrayToChoose::Small ) {
            if ( m_smallArray.start_index == m_smallArray.size ) {
                std::cout << "unable to add element to array 'Small': array is full" << std::endl;
                return false;
            }
            m_smallArray.data[ m_smallArray.start_index ] = element;
            ++m_smallArray.start_index;
            return true;
        }
        else if ( arrayToChoose == ArrayToChoose::Big ) {
            if ( m_bigArray.start_index == m_bigArray.size ) {
                std::cout << "unable to add element to array 'Big': array is full" << std::endl;
                return false;
            }
            m_bigArray.data[ m_bigArray.start_index ] = element;
            ++m_bigArray.start_index;
            return true;
        }
        return false;
    }

    void printArray( ArrayToChoose arrayToChoose ) {
        if ( arrayToChoose == ArrayToChoose::Small ) {
            if ( m_smallArray.start_index == 0 ) {
                std::cout << "no elements were found in array 'Small'" << std::endl;
            }
            else {
                for ( std::size_t i = 0; i < m_smallArray.start_index; ++i ) {
                    std::cout << "SmallArray[" << i << "]: '" << m_smallArray.data[ i ] << "'" << std::endl;
                }
            }
        }
        else if ( arrayToChoose == ArrayToChoose::Big ) {
            if ( m_bigArray.start_index == 0 ) {
                std::cout << "no elements were found in array 'Big'" << std::endl;
            }
            else {
                for ( std::size_t i = 0; i < m_bigArray.start_index; ++i ) {
                    std::cout << "BigArray[" << i << "]: '" << m_bigArray.data[ i ] << "'" << std::endl;
                }
            }
        }
    }

private:
    struct ArrayParams {
        ArrayParams() :
            data{ nullptr }, size{ 0U }, start_index{ 0U }
        {}

        ArrayParams( ArrayParams&& other ) :
            data( std::move( other.data ) ),
            size( std::move( other.size ) ),
            start_index( std::move( other.start_index ) )
        {
            other.data = nullptr;
            other.size = 0U;
            other.start_index = 0U;
        }

        ArrayParams& operator=( ArrayParams&& other ) {
            data = std::move( other.data );
            size = std::move( other.size );
            start_index = std::move( other.start_index );

            other.data = nullptr;
            other.size = 0U;
            other.start_index = 0U;
            return *this;
        }

        ArrayParams( const ArrayParams& other ) = delete;
        ArrayParams& operator=( const ArrayParams& other ) = delete;
        ~ArrayParams() = default;

        std::unique_ptr< T[] > data;
        std::size_t size;
        std::size_t start_index;
    };

    ArrayParams m_smallArray;
    ArrayParams m_bigArray;
};

int main() {
    try {
        ArrayAggregator< char, 1024 > aggregator;
        char symbol = 'A';
        aggregator.addElement( ArrayToChoose::Big, symbol );
        aggregator.printArray( ArrayToChoose::Big );
    }
    catch ( const std::bad_alloc& exception ) {
        std::cerr << "unable to allocate memory; exception:'" << exception.what() << "'" << std::endl;
    }
    catch ( ... ) {
        std::cerr << "unknown exception was caught" << std::endl;
    }
    return 0;
}